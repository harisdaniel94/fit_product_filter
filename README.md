# Project Description #

Mějme na stránce libovolný *seznam zboží* (např. knih, notebooků, CD, apod.) *libovolně formátovaný pomocí HTML*.
Celý seznam je obsažen v prvku s nějakým identifikátorem, každá položka je reprezentována pomocí libovolného
prvku (např. div) s **class="item"**. Prezentované zboží má pevný počet předem daných parametrů, které jsou zobrazeny
(např. název, autor, cena). V HTML kódu jsou tyto parametry reprezentovány pomocí libovolných elementů s atributem
class nastaveným na jméno parametru (např. cena může být v span class="cena" nebo v em class="cena" apod.) Kromě
těchto elementů může položku tvořit libovolný další kód (např. obrázek zboží) a položka může být libovolně
strukturovaná (údaje mohou být obsaženy v jiných elementech, např. v seznamu). 

Vytvořte funkci v JavaScriptu, která nad seznamem zboží zobrazí prvky pro jeho filtrování. Pomocí parametrů této
funkce bude možné zadat názvy parametrů zboží, které se sledují a jejich typ (řetězcový, numerický nebo výčtový).
Pro řetězcové atributy se zobrazí vstupní pole, které umožní zadat podřetězec, který musí být obsažen v hodnotě
daného parametru, aby příslušné zboží bylo zobrazeno. Pro numerické atributy bude možno zadat rozsah hodnot (od, do).
Pro výčtové parametry se nabídnou všechny různé hodnoty obsažené v HTML seznamu s tím, že bude možno vybrat
libovolný počet z nich.