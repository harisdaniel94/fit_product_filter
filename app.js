/**
 * Subject: WAP - Internet Applications
 * Project: Product filter (vyber zbozi)
 *  Author: Daniel Haris (xharis00)
 *    File: app.js
 */

/**
 * JSON for filter parameters, filled in function "addFilter()"
 * Structure:
 * {
 *   "product_parameter_string_type": {
 *     type: "string"
 *   },
 *   "product_parameter_numeric_type": {
 *     valid: Boolean,
 *     from: Integer,
 *     to: Integer,
 *     type: "number"
 *   },
 *   "product_parameter_enum_type": {
 *     type: "enum"
 *   }
 * }
 */
var filters = {};

/**
 * Function removes duplicates from an array
 */
Array.prototype.unique = function() {
    var a = [];
    for (var i=0, l=this.length; i<l; i++) {
        if (a.indexOf(this[i]) === -1) {
            a.push(this[i]);
        }
    }
    return a;
};

/**
 * Function capitalises the first letter of a string
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * Function returns array of all text nodes of an element using recursive search
 * @param elem given element
 * @return Array of text nodes
 */
function getTextContent(elem) {
    var texts = [];
    for (var i = 0; i < elem.childNodes.length; i++) {
        var child = elem.childNodes[i];
        switch(child.nodeType) {
			case 1:
                texts.push.apply(texts, getTextContent(child));
                break;
			case 3:
				if (child.data.trim()) {
				    texts.push(child.data.trim());
                }
		}
    }
    return texts;
}

/**
 * Function returns bool value depending on whether element has a given class name
 * @param elem given element
 * @param cls given class
 * @return boolean
 */
function hasClass(elem, cls) {
    return (' ' + elem.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

/**
 * Function returns element's parent element with class 'item'
 * @param elem given element
 * @return parent element with class 'item'
 */
function getParentItem(elem) {
	if (!hasClass(elem.parentElement, "item")) {
		return getParentItem(elem.parentElement);
	} else {
        return elem.parentElement;
	}
}

/**
 * Function returns list of items filtered by given text input filter rule
 * @param prodParam product parameter name
 * @return Array of product items
 */
function getItemsFilteredByText(prodParam) {
	var productItems = document.getElementsByClassName(prodParam),
	    inputElem = document.getElementById(prodParam + "_text"),
	    filteredItems = [], i;
	if (!inputElem.value) {
        for (i = 0; i < productItems.length; i++) {
            filteredItems.push(getParentItem(productItems[i]));
        }
    } else {
        for (i = 0; i < productItems.length; i++) {
            // case insensitive searching
            if (productItems[i].textContent.toLowerCase().indexOf(inputElem.value.toLowerCase()) >= 0) {
                filteredItems.push(getParentItem(productItems[i]));
            }
        }
    }
	return filteredItems;
}

/**
 * Function returns list of items filtered by given numeric input filter rule
 * @param prodParam product parameter name
 * @return Array of product items
 */
function getItemsFilteredByNumber(prodParam) {
	// variable definitions
    var inputFrom = document.getElementById(prodParam + "_from"),
        inputTo = document.getElementById(prodParam + "_to"),
        numFrom = parseFloat(inputFrom.value),
        numTo = parseFloat(inputTo.value),
        productItems = document.getElementsByClassName(prodParam),
        filteredItems = [];
    // input range validation
    if (!isNaN(numFrom) && numFrom <= filters[prodParam].to &&
        !isNaN(numTo) && numTo >= filters[prodParam].from) {
        filters[prodParam].valid = true;
        filters[prodParam].from = numFrom;
        filters[prodParam].to = numTo;
        inputFrom.style.backgroundColor = "";
        inputTo.style.backgroundColor = "";
    } else {
        filters[prodParam].valid = false;
        inputFrom.style.backgroundColor = "tomato";
        inputTo.style.backgroundColor = "tomato";
    }
	// if invalid range then return all items
	if (!filters[prodParam].valid) {
		for (var i = 0; i < productItems.length; i++) {
            filteredItems.push(getParentItem(productItems[i]));
        }
        return filteredItems;
    }
	// if valid range then return only those items which fits the range
    for (i = 0; i < productItems.length; i++) {
        var validNumbersPerProduct = [];
        // numeric product parameter could have more text nodes and not all could contain a text node
        // if so, find all numeric and check whether are in range
        for (var j = 0, textNodes = getTextContent(productItems[i]); j < textNodes.length; j++) {
            var matchResult = textNodes[j].match(/-?(\d)+(\.)?(\d)*/g);
            if (matchResult !== null) {
                validNumbersPerProduct.push.apply(validNumbersPerProduct, matchResult);
            }
        }
        // check whether all of collected numbers are within range
        // if all of the are out of range, hide that item
        for (j = 0; j < validNumbersPerProduct.length; j++) {
            if (validNumbersPerProduct[j] >= filters[prodParam].from &&
                validNumbersPerProduct[j] <= filters[prodParam].to) {
                filteredItems.push(getParentItem(productItems[i]));
            }
        }
    }
    return filteredItems;
}

/**
 * Function returns list of items filtered by given numeric input filter rule
 * @param prodParam product parameter name
 * @return Array of product items
 */
function getItemsFilteredByEnum(prodParam) {
    var enumContainer = document.getElementById(prodParam + "_container"),
        productItems = document.getElementsByClassName(prodParam),
        checkedCheckboxValues = [], filteredItems = [];
    // get filter keywords of checked checkboxes
    for (var i = 0, checkboxes = enumContainer.childNodes; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) checkedCheckboxValues.push(checkboxes[i].getAttribute("name"));
    }
    // if all checkboxes are disabled, return all product items
    if (checkedCheckboxValues.length === 0) {
        for (i = 0; i < productItems.length; i++) {
            filteredItems.push(getParentItem(productItems[i]));
        }
    } else {  // else return only those product items, which contain all checked checkboxes
        // filter content, for all product items iterate
        for (i = 0; i < productItems.length; i++) {
            // get all enum values per product item
            var itemEnumTexts = getTextContent(productItems[i]);
            for (var j = 0; j < checkedCheckboxValues.length; j++) {
                if (itemEnumTexts.indexOf(checkedCheckboxValues[j]) !== -1) {
                    filteredItems.push(getParentItem(productItems[i]));
                    break;
                }
            }
        }
    }
    return filteredItems;
}

/**
 * Function handles filter input changes and filters the HTML content
 */
function updateContent() {
    var productItems = document.getElementsByClassName("item"),
        filteredBy = { "string": [], "number": [], "enum": []},
        filterResults = { "string": [], "number": [], "enum": [] },
        types = ["string", "number", "enum"];
    // collect filter results as array of arrays for all types of filters
    for (var i = 0, keys = Object.keys(filters); i < keys.length; i++) {
        switch(filters[keys[i]].type) {
            case "string":
                filterResults["string"].push(getItemsFilteredByText(keys[i]));
                break;
            case "number":
                filterResults["number"].push(getItemsFilteredByNumber(keys[i]));
                break;
            case "enum":
                filterResults["enum"].push(getItemsFilteredByEnum(keys[i]));
        }
    }
    // merge array of arrays to an array of filter results by filter type
    for (i = 0; i < types.length; i++) {
        if (filterResults[types[i]].length > 0) {
            for (var j = 0; j < filterResults[types[i]].length; j++) {
                if (j === 0) {
                    filteredBy[types[i]].push.apply(filteredBy[types[i]], filterResults[types[i]][j]);
                } else {
                    filteredBy[types[i]] = filteredBy[types[i]].filter(function(n) {
                        return filterResults[types[i]][j].indexOf(n) !== -1;
                    });
                }
            }
        }
    }
    // get the intersection of all filters
    var filteredByAll = filteredBy["string"].filter(function(i) {
        var intersectionOfNumberAndEnum = filteredBy["number"].filter(function(j) {
            return filteredBy["enum"].indexOf(j) !== -1;
        });
        return intersectionOfNumberAndEnum.indexOf(i) !== -1;
    });
    // change web content by final filter rule
    for (i = 0; i < productItems.length; i++) {
        productItems[i].style.display = filteredByAll.indexOf(productItems[i]) !== -1 ? '' : 'none';
    }
}

/**
 * Function adds new product filter
 * @param prodParam product parameter name (class)
 * @param paramType product parameter type (string, )
 * @param filterLabel
 */
function addFilter(prodParam, paramType, filterLabel) {

    // paramType must be of these 3 values
    if (["string", "number", "enum"].indexOf(paramType) === -1) {
        alert("paramType must be 1 of ['string', 'number', 'enum'].\nYour actual paramType is '" + paramType + "'.");
        return;
    }
	var entries = document.getElementById("filter_entries");
	if (entries === null) {
		entries = document.createElement("div");
		entries.setAttribute("id", "filter_entries");
		document.body.insertBefore(entries, document.body.firstChild);
	}
	// add filter label
    var label = (filterLabel === undefined) ? prodParam.capitalize() : filterLabel + ":\u00A0";
	entries.appendChild(document.createTextNode(label));
	switch(paramType) {
        case "string":
            filters[prodParam] = { type : "string" };
			var strInput = document.createElement("input"),
			    textContainer = document.createElement("div");
			textContainer.setAttribute("class", "stringFilterContainer");
			strInput.setAttribute("id", prodParam + "_text");
			strInput.setAttribute("type", "text");
			strInput.setAttribute("onkeyup", "updateContent()");
			textContainer.appendChild(strInput);
			entries.appendChild(textContainer);
			break;
		case "number":
            filters[prodParam] = { from : 0, to : 100, valid : true, type : "number" };
			var numInputFrom = document.createElement("input"),
				numInputTo = document.createElement("input"),
			    rangeContainer = document.createElement("div");
            rangeContainer.setAttribute("class", "rangeFilterContainer");
			numInputFrom.setAttribute("id", prodParam + "_from");
			numInputFrom.setAttribute("type", "number");
			numInputFrom.setAttribute("value", "0");
            numInputFrom.setAttribute("onkeyup", "updateContent()");
			numInputTo.setAttribute("id", prodParam + "_to");
            numInputTo.setAttribute("type", "number");
            numInputTo.setAttribute("value", "50");
            numInputTo.setAttribute("onkeyup", "updateContent()");
            numInputTo.setAttribute("onmouseup", "updateContent()");
            rangeContainer.appendChild(numInputFrom);
            rangeContainer.appendChild(numInputTo);
			entries.appendChild(rangeContainer);
			break;
		case "enum":
		    filters[prodParam] = { type : "enum" };
			var enumElems = document.getElementsByClassName(prodParam),
                enumContainer = document.createElement("div"),
			    enumItems = [];
			enumContainer.setAttribute("id", prodParam + "_container");
            enumContainer.setAttribute("class", "enumFilterContainer");
            for (var i = 0; i < enumElems.length; i++) {
                enumItems.push.apply(enumItems, getTextContent(enumElems[i]));
            }
			enumItems = enumItems.unique();
			for (i = 0; i < enumItems.length; i++) {
				var newCheckbox = document.createElement("input"),
					enumLabel = document.createElement("label"),
					labelSpan = document.createElement("span"),
					labelText = document.createTextNode("\u00A0" + enumItems[i]),
					checkboxId = prodParam + "_" + i;
				newCheckbox.setAttribute("id", checkboxId);
				newCheckbox.setAttribute("type", "checkbox");
				newCheckbox.setAttribute("name", enumItems[i]);
				newCheckbox.setAttribute("onchange", "updateContent()");
				enumContainer.appendChild(newCheckbox);
				enumLabel.setAttribute("for", checkboxId);
                labelSpan.setAttribute("class", "checkbox_style");
                labelSpan.appendChild(labelText);
				enumLabel.appendChild(labelSpan);
				enumContainer.appendChild(enumLabel);
				enumContainer.appendChild(document.createElement("br"));
			}
			entries.appendChild(enumContainer);
	}
	entries.appendChild(document.createElement("br"));
}

/**
 * Function is called on web page load and adds filters via function "addFilter" defined above
 */
window.onload = function() {
    addFilter("name", "string");
    addFilter("description", "string");
    addFilter("price", "number");
    addFilter("onstock", "number", "On Stock");
    addFilter("color", "enum");
    updateContent();
};
